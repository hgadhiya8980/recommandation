from flask import Flask, render_template, request, redirect, url_for, flash
import os
from werkzeug.utils import secure_filename
import pickle
import numpy as np
from PIL import Image
import tensorflow
from tensorflow.keras.preprocessing import image
from tensorflow.keras.applications.resnet50 import ResNet50, preprocess_input
from tensorflow.keras.layers import GlobalMaxPooling2D
from sklearn.neighbors import NearestNeighbors
from numpy.linalg import norm

# Load model and data
embeddings_third = os.path.abspath("embeddings_third.pkl")
filenames_third = os.path.abspath("filenames_third.pkl")
feature_list = np.array(pickle.load(open(embeddings_third, 'rb')))
filenames = pickle.load(open(filenames_third, 'rb'))

# Configure Flask app
app = Flask(__name__)
app.config['SECRET_KEY'] = 'ujshgdhjzdgfbsyuhfgsjhfm'  # Set a strong secret key for security
app.config['UPLOAD_FOLDER'] = "static/upload/"  # Define upload folder within your app structure

model = ResNet50(weights='imagenet', include_top=False, input_shape=(224, 224, 3))
model.trainable = False
model = tensorflow.keras.Sequential([model, GlobalMaxPooling2D()])

# Allowed file extensions for images
ALLOWED_EXTENSIONS = ['jpg', 'jpeg', 'gif', 'png']

def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route('/')
def home():
    return render_template('index.html')

def feature_extraction(img_path, model):
    img = image.load_img(img_path, target_size=(224, 224))
    img_array = image.img_to_array(img)
    expanded_img_array = np.expand_dims(img_array, axis=0)
    preprocessed_img = preprocess_input(expanded_img_array)
    result = model.predict(preprocessed_img).flatten()
    normalized_result = result / norm(result)
    return normalized_result

def recommend(features, feature_list):
    neighbors = NearestNeighbors(n_neighbors=4, algorithm='brute', metric='euclidean')
    neighbors.fit(feature_list)
    distances, indices = neighbors.kneighbors([features])

    return indices
@app.route('/recommandation', methods=['GET', 'POST'])
def recommandation():
    mapping_dict = {}
    if 'file' not in request.files:
        flash('No file part', "danger")
        return redirect("/")
    file = request.files['file']
    if file.filename == '':
        flash('No selected file', "danger")
        return redirect("/")
    if file.filename is not None:
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file_path = os.path.join(app.config['UPLOAD_FOLDER'], filename)
            file.save(file_path)
            mapping_dict["input_path"] = file_path
            features = feature_extraction(file_path, model)
            indices = recommend(features, feature_list)
            recommended_filenames = [filenames[idx] for idx in indices[0]]  # Extract recommended filenames
            # recommended_filenames = [idx for idx in indices[0]]  # Extract recommended filenames

            mapping_dict["output_path"] = recommended_filenames
            flash('Image Recommandation successfully!', "success")
            return render_template("recommandation.html", mapping_dict=mapping_dict)
            # return render_template("uploaded_image.html", image_names = recommended_filenames)
        else:
            flash('Invalid file type. Only PNG, JPG, JPEG, and GIF files are allowed.')
            return redirect("/")
    else:
        flash('Error uploading file.')
        return redirect("/")

if __name__ == '__main__':
    app.run()